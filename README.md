# G�om�trie

Projet de mise en application de mes conaissances en mod�lisation g�om�trique des surfaces,

Apr�s avoir compl�t� mes conaissances par le suivi d'un __<a href="https://perso.liris.cnrs.fr/sbrandel/wiki/doku.php?id=ens:mif27" target="_blank">cours</a>__ en ligne propos� par "Sylvain Brandel",
ma�tre de conf�rences en master informatique � l'universit� Claude Bernard Lyon 1.


J'ai entrepris de r�aliser un <a href="https://forge.univ-lyon1.fr/m1if27/courbes-etu" target="_blank">TP sur la g�n�ration de courbes</a> issu de l'unit� d'enseignement "Synth�se d'image",
accesible en ligne, et avec le outils mis � disposition, tel que <a href="https://perso.liris.cnrs.fr/vincent.nivoliers/mif27/" target="_blank">gKit</a>.

