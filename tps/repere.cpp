
#include <stdio.h>
#include "window.h"

#include "mesh.h"
#include "wavefront.h"  // pour charger un objet au format .obj
#include "texture.h"

#include "orbiter.h"

#include "draw.h"        // pour dessiner du point de vue d'une camera
#include "vec.h"

Mesh mesh;
Mesh repere_mesh;
Orbiter camera;

/**
 * Construit le maillage de la courbe en fonction  d'un vecteur de sommets.
 */
void buildMesh(Mesh& mesh, std::vector<Point> sommets, bool double_sommet) {
    for (std::vector<Point>::iterator sommetIt = sommets.begin(); sommetIt != sommets.end(); sommetIt++) {
        Point sommet = *sommetIt;
        mesh.vertex(sommet);
        if (sommetIt != sommets.begin() && sommetIt + 1 != sommets.end() && double_sommet) {
            mesh.vertex(sommet);
        }
    }
}

/**
 * subdivision de la courbe pass�e en argument en utilisant le sch�ma de subdivision de Chaikin.
 */
std::vector<Point> subdiv(std::vector<Point> sommets, int nb_iterations) {
    for (int i = 0; i < nb_iterations; i++) {
        std::vector<Point> newSommets = std::vector<Point>();
        std::vector<Point>::iterator end = sommets.end();
        std::vector<Point>::iterator sommetIt = sommets.begin();
        while (++sommetIt != sommets.end()) {
            Point s1 = *(sommetIt - 1);
            Point s2 = *(sommetIt);

            Point newS1 = s1 + 0.25 * (s2 - s1);
            Point newS2 = s1 + 0.75 * (s2 - s1);

            newSommets.push_back(newS1);
            newSommets.push_back(newS2);
        }
        sommets.swap(newSommets);
        newSommets.clear();
    }
    return sommets;
}

/**
 * Construction des repaires le long de la courbe.
 */
std::vector<Point> buildReperes(std::vector<Point> sommets) {
    std::vector<Point> reperes = std::vector<Point>();
    // Un premier vecteur orthogonal � la courbe

    // Calculez le vecteur a\mathbf{a}a correspondant � l'axe de la courbe par la diff�rence des sommets du segment
    Point p1 = *sommets.begin();
    std::cout << "p1" << p1 << std::endl;
    Point p2 = *(sommets.begin() + 1);
    std::cout << "p2" << p2 << std::endl;
    Vector a = normalize(p2 - p1);
    std::cout << "a" << a << std::endl;
    // Prenez un vecteur v\mathbf{v}v, n'importe lequel pourvu qu'il ne soit pas
        // align� avec le premier segment de la courbe(voir point suivant).Par exemple
        // utilisez le vecteur(1, 0, 0)(1, 0, 0)(1, 0, 0) et s'il est align�, alors utilisez le
        // vecteur(0, 1, 0)(0, 1, 0)(0, 1, 0).
    Vector v = Vector(0, 0, -1);
    std::cout << "v" << v << std::endl;
    // Calculez le produit vectoriel de a et de v. Si
        // le vecteur obtenu est nul, c'est que les deux vecteurs sont align�s. Sinon
        // vous avez obtenu un vecteur d orthogonal � a et
        // v, et donc en particulier � a
    // Normalisez le vecteur obtenu.
    Vector d = normalize(cross(a, v));
    std::cout << "d" << d << std::endl;
    Point dp = Point(p1.x + d.x, p1.y + d.y, p1.z + d.z);
    std::cout << "dp" << dp << std::endl;
    reperes.push_back(p1);
    reperes.push_back(dp);

    // D�placer ce premier vecteur le long de la courbe
    std::vector<Point>::iterator end = sommets.end();
    std::cout << "end" << *(end - 1) << std::endl;
    Transform t = Transform();
    std::cout << "Transformations-------" << std::endl;
    std::vector<Point>::iterator sommetIt = sommets.begin();
    while (++sommetIt != sommets.end()) {
        std::cout << "a " << a << std::endl;
        p1 = *sommetIt;
        std::cout << "p1 " << p1 << std::endl;
        p2 = *(sommetIt - 1);
        std::cout << "p2 " << p2 << std::endl;
        Vector b = normalize(p2 - p1);
        std::cout << "b " << b << std::endl;
        t = Rotation(a, b);
        std::cout << "v " << v << std::endl;
        std::cout << "t " << t << std::endl;
        float vx = t.m[0][0] * v.x + t.m[0][1] * v.y + t.m[0][2] * v.z;
        float vy = t.m[1][0] * v.x + t.m[1][1] * v.y + t.m[1][2] * v.z;
        float vz = t.m[2][0] * v.x + t.m[2][1] * v.y + t.m[2][2] * v.z;
        v = Vector(vx, vy, vz);
        std::cout << "v" << v << std::endl;
        d = normalize(cross(b, v));
        dp = Point(p1.x + d.x, p1.y + d.y, p1.z + d.z);
        reperes.push_back(p1);
        reperes.push_back(dp);
        a = b;
    }
    return reperes;
}

// creation des objets openGL
int init()
{
    Point pmin, pmax;;

    std::vector<Point> sommets = std::vector<Point>();
    sommets.push_back(Point(0., 0., 0.));
    sommets.push_back(Point(10., 0., 0.));
    sommets.push_back(Point(10., 10., 0.));
    sommets.push_back(Point(10., 10., 10.));
    int nb_iterations = 4;
    sommets = subdiv(sommets, nb_iterations);

    std::vector<Point> reperes = buildReperes(sommets);
    repere_mesh = Mesh(GL_LINES);
    buildMesh(repere_mesh, reperes, false);
    repere_mesh.bounds(pmin, pmax);


    mesh = Mesh(GL_LINES);
    buildMesh(mesh, sommets, true);
    mesh.bounds(pmin, pmax);

    // regle le point de vue de la camera pour observer l'objet
    camera.lookat(pmin, pmax);

    // etat openGL par defaut
    glClearColor(0.2f, 0.2f, 0.2f, 1.f);        // couleur par defaut de la fenetre

    // etape 3 : configuration du pipeline.
    glClearDepth(1.f);                          // profondeur par defaut
    glDepthFunc(GL_LESS);                       // ztest, conserver l'intersection la plus proche de la camera
    glEnable(GL_DEPTH_TEST);                    // activer le ztest


    return 0;   // renvoyer 0 ras, pas d'erreur, sinon renvoyer -1
}

// affichage
int draw()
{
    // etape 2 : dessiner l'objet avec opengl

    // on commence par effacer la fenetre avant de dessiner quelquechose
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // on efface aussi le zbuffer

    // recupere les mouvements de la souris, utilise directement SDL2
    int mx, my;
    unsigned int mb = SDL_GetRelativeMouseState(&mx, &my);

    // deplace la camera
    if (mb & SDL_BUTTON(1))              // le bouton gauche est enfonce
        // tourne autour de l'objet
        camera.rotation(mx, my);

    else if (mb & SDL_BUTTON(3))         // le bouton droit est enfonce
        // approche / eloigne l'objet
        camera.move(mx);

    else if (mb & SDL_BUTTON(2))         // le bouton du milieu est enfonce
        // deplace le point de rotation
        camera.translation((float)mx / (float)window_width(), (float)my / (float)window_height());

    // passer la texture en parametre
    //mesh.draw()
    //draw(mesh, /* use position */ true, /* use texcoord */ false, /* use normal */ false, /* use color */ false, /* use material index*/ false);
    draw(mesh, camera);
    draw(repere_mesh, camera);
    return 1;   // on continue, renvoyer 0 pour sortir de l'application
}

// destruction des objets openGL
int quit()
{
    // etape 3 : detruire la description de l'objet
    mesh.release();
    repere_mesh.release();

    return 0;   // ras, pas d'erreur
}

int main(int argc, char** argv)
{
    // etape 1 : creer la fenetre
    Window window = create_window(1024, 640);
    if (window == NULL)
        return 1;       // erreur lors de la creation de la fenetre ou de l'init de sdl2

    // etape 2 : creer un contexte opengl pour pouvoir dessiner
    Context context = create_context(window);
    if (context == NULL)
        return 1;       // erreur lors de la creation du contexte opengl

    // etape 3 : creation des objets
    if (init() < 0)
    {
        printf("[error] init failed.\n");
        return 1;
    }

    // etape 4 : affichage de l'application, tant que la fenetre n'est pas fermee. ou que draw() ne renvoie pas 0
    run(window, draw);

    // etape 5 : nettoyage
    quit();
    release_context(context);
    release_window(window);
    return 0;
}