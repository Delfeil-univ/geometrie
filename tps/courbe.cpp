
#include <stdio.h>
#include "window.h"

#include "mesh.h"
#include "wavefront.h"  // pour charger un objet au format .obj
#include "texture.h"

#include "orbiter.h"

#include "draw.h"        // pour dessiner du point de vue d'une camera

Mesh mesh;
Orbiter camera;

/**
 * Construit le maillage de la courbe en fonction  d'un vecteur de sommets.
 */
void buildMesh(Mesh &mesh, std::vector<Point> sommets) {
    for (std::vector<Point>::iterator sommetIt = sommets.begin(); sommetIt != sommets.end(); sommetIt++) {
        Point sommet = *sommetIt;
        mesh.vertex(sommet);
        if (sommetIt != sommets.begin() && sommetIt + 1 != sommets.end()) {
            mesh.vertex(sommet);
        }
    }
}

// creation des objets openGL
int init()
{
    Point pmin, pmax;;
        // Définition des sommets
    std::vector<Point> sommets = std::vector<Point>();
    sommets.push_back(Point(0., 0., 0.));
    sommets.push_back(Point(1., 0., 0.));
    sommets.push_back(Point(1., 1., 0.));
    sommets.push_back(Point(1., 1., 1.));

        // Placer les sommets dans un maillage GL_LINES
    mesh = Mesh(GL_LINES);
    buildMesh(mesh, sommets);
    mesh.bounds(pmin, pmax);

    // regle le point de vue de la camera pour observer l'objet
    camera.lookat(pmin, pmax);

    // etat openGL par defaut
    glClearColor(0.2f, 0.2f, 0.2f, 1.f);        // couleur par defaut de la fenetre

    // etape 3 : configuration du pipeline.
    glClearDepth(1.f);                          // profondeur par defaut
    glDepthFunc(GL_LESS);                       // ztest, conserver l'intersection la plus proche de la camera
    glEnable(GL_DEPTH_TEST);                    // activer le ztest


    return 0;   // renvoyer 0 ras, pas d'erreur, sinon renvoyer -1
}

// affichage
int draw()
{
    // etape 2 : dessiner l'objet avec opengl

    // on commence par effacer la fenetre avant de dessiner quelquechose
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // on efface aussi le zbuffer

    // recupere les mouvements de la souris, utilise directement SDL2
    int mx, my;
    unsigned int mb = SDL_GetRelativeMouseState(&mx, &my);

    // deplace la camera
    if (mb & SDL_BUTTON(1))              // le bouton gauche est enfonce
        // tourne autour de l'objet
        camera.rotation(mx, my);

    else if (mb & SDL_BUTTON(3))         // le bouton droit est enfonce
        // approche / eloigne l'objet
        camera.move(mx);

    else if (mb & SDL_BUTTON(2))         // le bouton du milieu est enfonce
        // deplace le point de rotation
        camera.translation((float)mx / (float)window_width(), (float)my / (float)window_height());

    // passer la texture en parametre 
    draw(mesh, camera);
    return 1;   // on continue, renvoyer 0 pour sortir de l'application
}

// destruction des objets openGL
int quit()
{
    // etape 3 : detruire la description de l'objet
    mesh.release();

    return 0;   // ras, pas d'erreur
}

int main(int argc, char** argv)
{
    // etape 1 : creer la fenetre
    Window window = create_window(1024, 640);
    if (window == NULL)
        return 1;       // erreur lors de la creation de la fenetre ou de l'init de sdl2

    // etape 2 : creer un contexte opengl pour pouvoir dessiner
    Context context = create_context(window);
    if (context == NULL)
        return 1;       // erreur lors de la creation du contexte opengl

    // etape 3 : creation des objets
    if (init() < 0)
    {
        printf("[error] init failed.\n");
        return 1;
    }

    // etape 4 : affichage de l'application, tant que la fenetre n'est pas fermee. ou que draw() ne renvoie pas 0
    run(window, draw);

    // etape 5 : nettoyage
    quit();
    release_context(context);
    release_window(window);
    return 0;
}